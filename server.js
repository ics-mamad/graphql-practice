const express = require('express');
const {graphqlHTTP} = require('express-graphql');
const DB = require('./db');
const schema = require('./schemas/schema')
const root = require('./roots/userRoot')

DB();

const app = express();

app.use('/graphql', graphqlHTTP({
    graphiql: true,
    schema: schema,
    rootValue: root
}));

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
