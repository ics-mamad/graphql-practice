const UserSchema = require('../Models/usersSchema');
const axios = require('axios');

const root = {
    
    getPosts: async () => {
        const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
        return res.data;
    },
    createUser: async ({input}) => {
        try {
            const user = new UserSchema(input);
            await user.save();
            return user;
        } catch (error) {
            throw new Error("Failed to create user.");
        }
    },
    deleteUser: async ({_id}) => {
        try {
            const user = await UserSchema.findByIdAndDelete(_id);
            return user;
        } catch (error) {
            throw new Error("Failed to delete user.");
        }
    },
    updateUser:async ({_id,input})=>{
        try {
            const user = await UserSchema.findByIdAndUpdate(_id,input);
            return user;
        } catch (error) {
            throw new Error("Failed to update user.");
        }
    }
};

module.exports = root