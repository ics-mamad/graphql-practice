const {buildSchema} = require('graphql');

const schema = buildSchema(`

    type User {
        name: String
        age: Int
        gender: String
        email: String
        password: String
    }

    input UserInput {
        name: String
        age: Int
        gender: String
        email: String
        password: String
    }

    type Query {
        hello: String
    }

    type Mutation {
        msg(mymsg: String): String
        createUser(input: UserInput!): User
        updateUser(_id: String!, input: UserInput): User
        deleteUser(_id: String!):User
    }

    type Post {
        userId: Int
        id: Int
        title: String
        body: String
    }
`);

module.exports = schema